# frozen_string_literal: true

require 'spec_helper'
require 'rspec/rails'
require 'devise_token_auth'
require 'pundit/rspec'
require 'factory_bot_rails'
require 'action_controller/railtie'
require_relative 'support/requests/auth_helper'
require_relative 'support/controller_macros'
require File.expand_path('../config/environment', __dir__)
require "trailblazer/test/deprecation/operation/assertions"

# Prevent database truncation if the environment is production
unless Rails.env.test?
  abort('The Rails environment is running in production mode!')
end

begin
  ActiveRecord::Migration.maintain_test_schema!
rescue ActiveRecord::PendingMigrationError => e
  puts e.to_s.strip
  exit 1
end

RSpec.configure do |config|
  config.fixture_path = "#{::Rails.root}/spec/fixtures"

  config.include FactoryBot::Syntax::Methods
  # config.before( scope :suite ) do
  #   FactoryBot.find_definitions
  # end

  config.include Devise::Test::ControllerHelpers, type: :controller
  config.extend ControllerMacros, type: :controller
  config.include Requests::AuthHelpers::Includables, type: :request
  config.extend Requests::AuthHelpers::Extensions, type: :request
  include Trailblazer::Test::Assertions
  include Trailblazer::Test::Operation::Assertions
  include Trailblazer::Test::Deprecation::Operation::Assertions
  include Trailblazer::Test::Operation::PolicyAssertions
  config.use_transactional_fixtures = true

  config.infer_spec_type_from_file_location!

  config.filter_rails_from_backtrace!
end
