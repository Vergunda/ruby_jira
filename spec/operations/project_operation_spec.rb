require 'rails_helper'

RSpec.describe 'Projects Controller requests', type: :operation do

  context "admin context" do

    let(:admin) { create :user, :admin }
    let(:title) { Faker::App.name }
    let!(:projects) { create_list :project, 3 }

    describe "Project::Create" do
      it "Should return success " do
        result = Project::Operation::Create.call(params: { title: title }, current_user: admin )
        assert result.success?
      end
      it "Should return return equal_title " do
        result = Project::Operation::Create.call(params: { title: title }, current_user: admin )
        assert_equal title, result['model'].title
      end
      it "Should return success user_id equals current_user.id" do
        result = Project::Operation::Create.call(params: { title: title }, current_user: admin )
        assert_equal admin.id, result['model'].user_id
      end
      it "Should return error if title not valid " do
        result = Project::Operation::Create.call(params: { title: '' }, current_user: admin )
        assert result.failure?
      end
    end

    describe "Project::Update" do

      it "Should return success " do
        result = Project::Operation::Update.call(params: { title: 'new_title', id: Project.last.id },
                                                 current_user: admin)
        assert result.success?
      end
      it "Should return error if title not valid " do
        result = Project::Operation::Update.call(params: { title: '', id: Project.last.id },
                                                 current_user: admin)
        assert result.failure?
      end
      it "Should return return new_title " do
        result = Project::Operation::Update.call(params: { title: 'new_title', id: Project.last.id },
                                                  current_user: admin)
        assert_equal 'new_title', result['model'].title
      end
      it "Should return updated_by user_id equals current_user.id" do
        result = Project::Operation::Update.call(params: { title: 'new_title', id: Project.last.id },
                                                 current_user: admin)
        assert_equal admin.id, result['model'].updated_by
      end
    end

    describe "Project::Show" do

      it "Should return success" do
        result = Project::Operation::Show.call(params: {id: Project.last.id}, current_user: admin)
        assert result.success?
      end

      it "Should return failure if project_id does not exist" do
        result = Project::Operation::Show.call(params: {id: 123124}, current_user: admin)
        assert result.failure?
      end

      it "Should return equal project" do
        result = Project::Operation::Show.call(params: {id: Project.last.id}, current_user: admin)
        assert_equal Project.last.id, result['model'].id
        assert_equal Project.last.title, result['model'].title
      end
    end

    describe "Project::Destroy" do

      it "Should return success" do
        result = Project::Operation::Destroy.call(params: {id: Project.last.id}, current_user: admin)
        assert result.success?
      end

      it "Should return failure if project_id does not exist" do
        result = Project::Operation::Destroy.call(params: {id: 123124}, current_user: admin)
        assert result.failure?
      end

      it "Should return deleted project" do
        @deleted_project = Project.last
        result = Project::Operation::Destroy.call(params: {id: Project.last.id}, current_user: admin)
        assert_equal @deleted_project.id, result['model'].id
        assert_equal @deleted_project.title, result['model'].title
      end

      it "Should return deleted project" do
        @deleted_project = Project.last
        Project::Operation::Destroy.call(params: {id: Project.last.id}, current_user: admin)
        assert Project.find_by(id: @deleted_project.id).nil?
      end
    end

    describe "Project::Index" do

      it "Should return success" do
        result = Project::Operation::Index.call(params: {sort_by: 'title', order: 'acs'}, current_user: admin)
        assert result.success?
      end

      it "Should return sorted by title list" do
        result = Project::Operation::Index.call(params: {sort_by: 'title', order: 'acs'}, current_user: admin)
        assert_equal Project.sorted_by_title('asc'), result['model']
      end

      it "Should return equal project" do
        result = Project::Operation::Index.call(params: {sort_by: 'title', order: 'desc'}, current_user: admin)
        assert_equal Project.sorted_by_title('desc'), result['model']
      end

      it "Should return sorted by title list" do
        result = Project::Operation::Index.call(params: {}, current_user: admin)
        assert_equal Project.ordered_projects, result['model']
      end
    end

  end

  context "user context" do

    let(:user) { create :user }
    let(:title) { Faker::App.name }
    let!(:projects) { create_list :project, 3 }

    describe "Project::Create" do
      it "Should return failure " do
        result = Project::Operation::Create.call(params: { title: title }, current_user: user )
        assert result.failure?
      end
      it "Should return return policy failure " do
        result = Project::Operation::Create.call(params: { title: title }, current_user: user )
        result['result.policy.default'].failure?
      end
    end

    describe "Project::Update" do

      it "Should return failure " do
        result = Project::Operation::Update.call(params: { title: title }, current_user: user )
        assert result.failure?
      end
    end

    describe "Project::Show" do

      it "Should return success" do
        result = Project::Operation::Show.call(params: {id: Project.last.id}, current_user: user)
        assert result.success?
      end

      it "Should return failure if project_id does not exist" do
        result = Project::Operation::Show.call(params: {id: 123124}, current_user: user)
        assert result.failure?
      end

      it "Should return equal project" do
        result = Project::Operation::Show.call(params: {id: Project.last.id}, current_user: user)
        assert_equal Project.last.id, result['model'].id
        assert_equal Project.last.title, result['model'].title
      end
    end

    describe "Project::Destroy" do

      it "Should return failure " do
        result = Project::Operation::Destroy.call(params: { title: title }, current_user: user )
        assert result.failure?
      end
    end

    describe "Project::Index" do

      it "Should return success" do
        result = Project::Operation::Index.call(params: {sort_by: 'title', order: 'acs'}, current_user: user)
        assert result.success?
      end

      it "Should return sorted by title list" do
        result = Project::Operation::Index.call(params: {sort_by: 'title', order: 'acs'}, current_user: user)
        assert_equal Project.sorted_by_title('asc'), result['model']
      end

      it "Should return equal project" do
        result = Project::Operation::Index.call(params: {sort_by: 'title', order: 'desc'}, current_user: user)
        assert_equal Project.sorted_by_title('desc'), result['model']
      end

      it "Should return sorted by title list" do
        result = Project::Operation::Index.call(params: {}, current_user: user)
        assert_equal Project.ordered_projects, result['model']
      end
    end

  end
end