FactoryBot.define do
  factory :project do
    title { Faker::App.name }
    user
    factory :project_with_task do
      after :create do |project, _|
        create :task, project: project
      end
    end
  end
end