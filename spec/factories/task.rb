FactoryBot.define do
  factory :task do
    title { Faker::Lorem.word}
    description { Faker::Lorem.sentence }
    done { Faker::Boolean }
    association :project, factory: :project
    association :user, factory: :user
  end
end