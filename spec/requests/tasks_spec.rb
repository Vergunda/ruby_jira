require 'rails_helper'

RSpec.describe 'Task controller', type: :request do
  context "Admin context" do

    let(:admin) { create :user, :admin }
    sign_in(:admin)

    let!(:projects) do
      create_list :project_with_task, 3
    end

    describe 'index tasks' do

      before do
        get "/projects/#{Project.last.id}/tasks"
      end

      it 'return success' do
        expect(response).to have_http_status :success
      end

      it 'title should return all tasks' do
        expect(JSON.parse(response.body)['tasks'].size).to eql(1)
      end
    end

    describe 'show task' do

      let(:user) { create :user }
      let(:title) { Faker::App.name }
      sign_in(:user)

      before do
        get "/projects/#{Project.last.id}/tasks/#{Project.last.tasks[0].id}"
      end

      it 'return success' do
        expect(response).to have_http_status :success
      end

      it 'title should return all tasks' do
        expect(JSON.parse(response.body)['id']).to eql(Project.last.tasks[0].id)
      end
    end

    describe 'create task' do

      let(:title) { Faker::App.name }
      let(:description) { Faker::Lorem.sentence }

      before do
        post "/projects/#{Project.last.id}/tasks/", params: {
             title: title,
             description: description
        }
      end

      it 'return success' do
        expect(response).to have_http_status :success
      end

      it "should return created task" do
        expect(JSON.parse(response.body)['title']).to eql(title)
        expect(JSON.parse(response.body)['description']).to eql(description)
      end

      it "Should add to tasklist" do
        expect(JSON.parse(response.body)['title']).to eql(Task.last.title)
      end
    end

    describe 'destroy task' do
      let(:deleting_task) { Task.last }

      before do
        delete "/projects/#{deleting_task.project_id}/tasks/#{deleting_task.id}"
      end

      it 'return success' do
        expect(response).to have_http_status :success
      end

      it 'Should return deleted task' do
        expect(JSON.parse(response.body)['id']).to eql(deleting_task.id)
      end

      it 'check deleted project = nul' do
        expect(Task.find_by(id: deleting_task.id)). to eql(nil)
      end
    end

  end

  context "User context" do

    let(:user) { create :user }
    sign_in(:user)

    let!(:projects) do
      create_list :project_with_task, 3
    end

    describe 'index tasks' do

      before do
        get "/projects/#{Project.last.id}/tasks"
      end

      it 'return success' do
        expect(response).to have_http_status :success
      end

      it 'title should return all tasks' do
        expect(JSON.parse(response.body)['tasks'].size).to eql(1)
      end
    end

    describe 'show task' do

      let(:user) { create :user }
      let(:title) { Faker::App.name }
      sign_in(:user)

      before do
        get "/projects/#{Project.last.id}/tasks/#{Project.last.tasks[0].id}"
      end

      it 'return success' do
        expect(response).to have_http_status :success
      end

      it 'title should return all tasks' do
        expect(JSON.parse(response.body)['id']).to eql(Project.last.tasks[0].id)
      end
    end

    describe 'create task' do

      let(:title) { Faker::App.name }
      let(:description) { Faker::Lorem.sentence }

      before do
        post "/projects/#{Project.last.id}/tasks/", params: {
            project_id: Project.last.id,
            task: { title: title, description: description }
        }
      end

      it 'return success have_http_status(403)' do
        expect(response).to have_http_status(403)
      end

      it "should return not allow to create this task" do
        # expect(JSON.parse(response.body)['errors']).to eql("not allowed to create? this Task")
      end
    end

    describe 'destroy task' do
      let(:deleting_task) { Task.last }

      before do
        delete "/projects/#{deleting_task.project_id}/tasks/#{deleting_task.id}"
      end

      it 'return success have_http_status(403)' do
        expect(response).to have_http_status(403)
      end

      it "should return not allow to destroy this task" do
        # expect(JSON.parse(response.body)['errors']).to eql("not allowed to destroy? this Task")
      end
    end

  end
end