require 'rails_helper'

RSpec.describe 'Projects Controller requests', type: :request do

  def parse_body(response, attribute_chain)
    body = JSON.parse(response.body)
    body.dig(*attribute_chain)
  end

  context "Admin context" do

    let(:admin) { create :user, :admin }
    sign_in(:admin)
    let!(:projects) { create_list :project, 3 }

    describe 'index project' do

      before do
        get '/projects?sort_by=title&order=asc'
      end

      it 'return success' do
        expect(response).to have_http_status :success
      end

      it 'return http status 200' do
        expect(response).to have_http_status(200)
      end

      it 'title should return all projects in array' do
        expect(JSON.parse(response.body)['projects'].pluck('id', 'title')).to eql(Project.sorted_by_title.pluck('id', 'title'))
      end
    end

    describe 'show project' do

      before do
        get "/projects/#{Project.last.id}"
      end

      it 'return http status 200' do
        expect(response).to have_http_status(200)
      end

      it 'title should return all projects in array' do
        id = parse_body(response, ['id'])
        expect(id).to eql(Project.last.id)
      end
    end

    describe 'create project' do

      let(:title) { Faker::App.name }

      before do
        post '/projects/', params: {title: title}
      end

      it 'return success' do
        expect(response).to have_http_status :success
      end

      it 'title should be eql title in query' do
        expect(JSON.parse(response.body)['title']).to eql(title)
      end
    end

    describe 'update project' do

      let(:title) { Faker::App.name }
      let(:changing_project) { Project.last }

      before do
        put "/projects/#{changing_project.id}", params: {title: 'new title'}
      end

      it 'return success' do
        expect(response).to have_http_status :success
      end

      it 'title should be eql title in query' do
        expect(JSON.parse(response.body)['title']).to eql('new title')
      end
    end

    describe 'destroy project' do

      let(:deleting_project) { Project.last }

      before do
        delete "/projects/#{deleting_project.id}"
      end

      it 'return success' do
        expect(response).to have_http_status :success
      end

      it 'Should return deleted project' do
        expect(JSON.parse(response.body)['id']).to eql(deleting_project.id)
      end

      it 'check projects size' do
        expect(Project.all.size).to eql(2)
      end

      it 'check deleted project = nul' do
        expect(Project.find_by(id: deleting_project.id)).to eql(nil)
      end

    end
  end

  context "User context" do

    let(:user) { create :user }
    sign_in(:user)
    let!(:projects) { create_list :project, 3 }

    describe 'index project' do

      before do
        get '/projects?sort_by=title&order=asc'
      end

      it 'return http status 200' do
        expect(response).to have_http_status(200)
      end

      it 'title should return all projects in array' do
        expect(JSON.parse(response.body)['projects'].pluck('id', 'title')).to eql(Project.sorted_by_title.pluck('id', 'title'))
      end
    end

    describe 'show project' do

      before do
        get "/projects/#{Project.last.id}"
      end

      it 'return http status 200' do
        expect(response).to have_http_status(200)
      end

      it 'title should return all projects in array' do
        expect(JSON.parse(response.body)['id']).to eql(Project.last.id)
      end
    end

    describe 'create project' do

      let(:title) { Faker::App.name }

      before do
        post '/projects/', params: {title: title}
      end

      it 'return http status 403' do
        expect(response).to have_http_status(403)
      end

      it 'should return "not allowed to create? this Project"' do
        # expect(JSON.parse(response.body)['errors']).to eql("not allowed to create? this Project")
      end
    end

    describe 'update project' do

      let(:title) { Faker::App.name }
      let(:changing_project) { Project.last }

      before do
        put "/projects/#{changing_project.id}", params: {project: {title: 'new title'}}
      end

      it 'return http status 403' do
        expect(response).to have_http_status(403)
      end

      it 'should return "not allowed to update? this Project"' do
        # expect(JSON.parse(response.body)['errors']).to eql("not allowed to update? this Project")
      end
    end

    describe 'destroy project' do

      let(:deleting_project) { Project.last }

      before do
        delete "/projects/#{deleting_project.id}"
      end

      it 'return http status 403' do
        expect(response).to have_http_status(403)
      end

      it 'should return "not allowed to destroy? this Project"' do
        # expect(JSON.parse(response.body)['errors']).to eql("not allowed to destroy? this Project")
      end

    end
  end

end