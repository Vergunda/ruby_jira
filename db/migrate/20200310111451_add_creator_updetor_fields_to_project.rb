class AddCreatorUpdetorFieldsToProject < ActiveRecord::Migration[6.0]
  def change
    add_column :projects, :updated_by, :integer
    add_reference :projects, :user, foreign_key: true
  end
end
