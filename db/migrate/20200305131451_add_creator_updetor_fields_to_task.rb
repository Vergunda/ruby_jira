class AddCreatorUpdetorFieldsToTask < ActiveRecord::Migration[6.0]
  def change
    add_column :tasks, :updated_by, :integer
    add_reference :tasks, :user, foreign_key: true
  end
end
