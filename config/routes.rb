Rails.application.routes.draw do

  mount_devise_token_auth_for 'User', at: 'auth', controllers: {
    # sessions: 'users/sessions'
    #
  }

  require 'sidekiq/web'
  mount Sidekiq::Web => '/sidekiq'
    # root to: 'projects#index'
  resources :projects do
    resources :tasks
  end

  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
