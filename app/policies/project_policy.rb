class ProjectPolicy < ApplicationPolicy

  def index?
    true
  end

  def show?
    true
  end

  def edit?
    user.role == 'admin'
  end

  def new?
    user.role == 'admin'
  end

  def create?
    new?
  end

  def update?
    edit?
  end

  def destroy?
    edit?
  end

  class Scope < Scope
    def resolve
      scope.all
    end
  end
end
