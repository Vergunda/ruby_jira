module Task::Representer
  class Index < Representable::Decorator
    include Representable::Hash

    collection :tasks, decorator: Task::Representer::Show, exec_context: :decorator

    def tasks
      represented
    end

  end
end