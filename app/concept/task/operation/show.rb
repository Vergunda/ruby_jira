module Task::Operation
  class Show < Trailblazer::Operation
    step   Model(Task, :find_by)
    fail  :log_error!
    pass :serialized_model!

    def log_error!(_options, **)
      puts 'some errors'
    end

    def serialized_model!(options, model:, **)
      options[:serialized_model] = Task::Representer::Show.new(model).to_hash
    end
  end
end