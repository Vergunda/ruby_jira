module Task::Operation
  class Update < Trailblazer::Operation
    step   Model(Task, :find_by)
    step   Policy::Pundit(TaskPolicy, :update?)
    step   Contract::Build(constant: Task::Contract::Update)
    step   Contract::Validate()
    step  :assign_updated_by!
    step   Contract::Persist()
    step  :send_update_task_email!
    fail  :log_error!
    pass :serialized_model!

    def log_error!(_options, model:, **)
      puts 'error'
    end

    def serialized_model!(options, model:, **)
      options[:serialized_model] = Task::Representer::Show.new(model).to_hash
    end

    def send_update_task_email!(options, model:,**)
      SendUpdateTaskEmailJob.perform_later(model.title, model.user_id, model.updated_by)
    end

    def assign_updated_by!(options, model:, current_user:, **)
      model.updated_by = current_user.id
    end
  end
end