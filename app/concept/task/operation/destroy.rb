module Task::Operation
  class Destroy < Trailblazer::Operation
    step Model(Task, :find_by)
    step Policy::Pundit(TaskPolicy, :destroy?)
    step :model_destroy!
    fail :log_error!
    pass :serialized_model!

    def log_error!(_options, **)
      puts 'some errors'
    end

    def serialized_model!(options, model:, **)
      options[:serialized_model] = Project::Representer::Show.new(model).to_hash
    end

    def model_destroy!(_options, model:, **)
      model.destroy
    end
  end
end
