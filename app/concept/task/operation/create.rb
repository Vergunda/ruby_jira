module Task::Operation
  class Create < Trailblazer::Operation
    step   Model(Task, :new)
    step   Policy::Pundit(TaskPolicy, :create?)
    step   :add_project_id!
    step   Contract::Build(constant: Task::Contract::Create)
    step   :assign_current_user!
    step   Contract::Validate()
    step   Contract::Persist()
    fail  :log_error!
    pass :serialized_model!

    def log_error!(_options, **)
      Task::Representer::Show.new( "not allowed to create? this Project")
    end

    def serialized_model!(options, model:, **)
      options[:serialized_model] = Task::Representer::Show.new(model).to_hash
    end

    def assign_current_user!(_options, model:, current_user:, **)
      model.user_id = current_user.id
    end

    def add_project_id!(_options, model:, params:, **)
        model['project_id'] = params['project_id']
    end
  end
end