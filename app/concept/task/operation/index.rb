module Task::Operation
  class Index < Trailblazer::Operation
    step :model!
    fail  :log_error!
    pass :serialized_model!

    def model!(options, params:, **)
      options['model'] = Task.where(project_id: params['project_id'])
    end

    def log_error!(_options, **)
      puts 'some errors'
    end

    def serialized_model!(options, model:, **)
      options[:serialized_model] = Task::Representer::Index.new(model).to_hash
    end
  end
end