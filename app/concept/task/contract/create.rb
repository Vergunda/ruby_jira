module Task::Contract
  class Create < Reform::Form

    property :title
    property :description

    validates :title, length: 1..20
  end
end