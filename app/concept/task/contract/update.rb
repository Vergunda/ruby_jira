module Task::Contract
  class Update < Reform::Form
    property :title

    validates :title,  length: 1..20
  end
end