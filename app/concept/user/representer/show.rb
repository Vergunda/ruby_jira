module User::Representer
  class Show < Representable::Decorator
    include Representable::Hash

    property :id
    property :email
    property :created_at
    property :role

  end
end