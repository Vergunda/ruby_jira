module Project::Representer
  class Error < Representable::Decorator

    property :error_message

  end
end