module Project::Representer
  class Index < Representable::Decorator
    include Representable::Hash

    collection :projects, decorator: Project::Representer::Show, exec_context: :decorator

    def projects
      represented
    end
  end
end