module Project::Representer
  class Show < Representable::Decorator
    include Representable::Hash

    property :id
    property :title
    property :created_at
    property :user, decorator: User::Representer::Show, class: User

  end
end