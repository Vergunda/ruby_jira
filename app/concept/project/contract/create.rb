module Project::Contract
  class Create < Reform::Form

    property :title

    validates :title, length: 1..20
  end
end