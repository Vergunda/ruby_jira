module Project::Operation
  class Show < Trailblazer::Operation
    step   Model(Project, :find_by)
    pass :serialized_model!

    def serialized_model!(options, model:, **)
      options[:serialized_model] = Project::Representer::Show.new(model).to_hash
    end
  end
end