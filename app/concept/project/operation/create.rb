module Project::Operation
  class Create < Trailblazer::Operation
    step   Model(Project, :new)
    step  :assign_current_user!
    step   Policy::Pundit(ProjectPolicy, :create?)
    step   Contract::Build(constant: Project::Contract::Create)
    step   Contract::Validate()
    step   Contract::Persist()
    fail  :log_error!
    pass :serialized_model!

    def log_error!(_options, **)
      Project::Representer::Show.new( "not allowed to create? this Project")
    end

    def serialized_model!(options, model:, **)
      options[:serialized_model] = Project::Representer::Show.new(model).to_hash
    end

    def assign_current_user!(_options, model:, current_user:, **)
      model.user_id = current_user.id
    end
  end
end