module Project::Operation
  class Index < Trailblazer::Operation
    step :model!
    pass :serialized_model!

    def model!(options, **)
      options['model'] = Project.index_request(options['params'])
    end

    def serialized_model!(options, model:, **)
      options[:serialized_model] = Project::Representer::Index.new(model).to_hash
    end
  end
end