module Project::Operation
  class Update < Trailblazer::Operation
    step   Model(Project, :find_by)
    step   :assign_updated_by!
    step   Policy::Pundit(ProjectPolicy, :update?)
    step   Contract::Build(constant: Project::Contract::Update)
    step   Contract::Validate()
    step   Contract::Persist()
    fail   :log_error!
    pass :serialized_model!

    def log_error!(_options, **)
      puts 'some errors'
    end

    def serialized_model!(options, model:, **)
      options[:serialized_model] = Project::Representer::Show.new(model).to_hash
    end

    def assign_updated_by!(_options, model:, current_user:, **)
      model.updated_by = current_user.id
    end
  end
end