# frozen_string_literal: true

class ApplicationController < ActionController::API
  include DeviseTokenAuth::Concerns::SetUserByToken
  include SimpleEndpoint::Controller
  include Trailblazer::Rails::Controller

  private

  def default_handler
    {
      created: ->(result) { render json: result[:serialized_model], status: :created },
      success: ->(result) { render json: result[:serialized_model], status: :ok },
      invalid: ->(result) { render json: {errors: result[:model]&.errors&.full_messages}, status: :unprocessable_entity },
      not_found: ->(_result) { head(:not_found) },
      no_content: ->(_result) { head(:no_content) },
      unauthenticated: ->(_result) { head(:forbidden) }
    }
  end

  def default_cases
    {
      present: ->(result) { result.success? && result['present'] },
      created: ->(result) { result.success? && result['model.action'] == :new },
      no_content: ->(result) { result.success? && result['model.response'] == :no_content },
      success: ->(result) { result.success? },
      not_found: ->(result) { result.failure? && result['result.model'] && result['result.model'].failure? },
      unauthenticated: ->(result) { result.failure? && result['result.policy.default'] && result['result.policy.default'].failure? },
      invalid: ->(result) { result.failure? }
    }
  end
end
