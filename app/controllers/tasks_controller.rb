class TasksController < ApiController

  def index
    endpoint operation: Task::Operation::Index,
             options: { params: params, current_user: current_user }
  end

  def show;
    endpoint operation: Task::Operation::Show,
             options: { params: params, current_user: current_user }
  end

  def create
    endpoint operation: Task::Operation::Create,
             options: { params: params, current_user: current_user }
  end

  def update
    endpoint operation: Task::Operation::Update,
             options: { params: params, current_user: current_user }
  end

  def destroy
    endpoint operation: Task::Operation::Destroy,
             options: { params: params, current_user: current_user }
  end

  private

  def set_task
    @task = @project.tasks.find_by(id: params[:id])
  end

  def set_project
    @project = Project.find_by(id: params[:project_id])
  end

  def task_params
   params.require(:task).permit(:title, :description)
  end
end
