class ProjectsController < ApiController

  def index
    endpoint operation: Project::Operation::Index,
             options: { params: params, current_user: current_user }
  end

  def show
    endpoint operation: Project::Operation::Show,
             options: { params: params, current_user: current_user }
  end

  def create
    endpoint operation: Project::Operation::Create,
             options: { params: params, current_user: current_user }
  end

  def update
    endpoint operation: Project::Operation::Update,
             options: { params: params, current_user: current_user }
  end

  def destroy
    endpoint operation: Project::Operation::Destroy,
             options: { params: params, current_user: current_user }
  end

  private

    def project_params
    params.require(:project).permit( :title)
  end
end
