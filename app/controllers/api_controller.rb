class ApiController < ApplicationController
  before_action :authenticate_user!

  include Pundit

  rescue_from Pundit::NotAuthorizedError do |exception|
    render json: { errors: exception.message }, status: :forbidden
  end




end