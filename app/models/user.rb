# frozen_string_literal: true

class User < ActiveRecord::Base
  include DeviseTokenAuth::Concerns::User
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :trackable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  validates :email, uniqueness: true
  has_many :tasks
  has_many :projects
  enum role: %i[user admin]

  def admin?
    self.role == 'admin'
  end

  def user?
    role == 'user'
  end
end
