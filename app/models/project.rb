class Project < ApplicationRecord
  has_many :tasks, dependent: :destroy
  belongs_to :user

  def self.filtered_by_title(title = nil)
    params_filter('title', title)
  end

  def self.ordered_projects
    sort_by_task_quantity
  end

  def self.sorted_by_creating_date(type = nil)
    sort_by_create_at(type)
  end

  def self.sorted_by_title(type = nil)
    sort_by_title(type)
  end

  def self.index_request(params)
    case params[:sort_by]
    when 'title'
      sort_by_title(params[:order])
    when 'created_at'
      sorted_by_creating_date(params[:order])
    else
      sort_by_task_quantity
    end
  end

  scope :params_filter, (lambda do |attribute, value|
    where("#{attribute} ILIKE ?", "%#{value}%") if value
  end)

  scope :sort_by_task_quantity, -> {
    left_joins(:tasks).group("projects.id").order("count(tasks.id) DESC")
  }

  scope :sort_by_title, (lambda do |type|
    type == 'desc' ? order(title: :desc) : order(:title)
  end)

  scope :sort_by_create_at, (lambda do |type|
    type == 'desc' ? order(created_at: :desc) : order(:created_at)
  end)

end
