class ProjectMailer < ApplicationMailer
  default from: 'notifications@example.com'

  def new_project_created(user, project)
    @project = project
    @user = user
    @url  = 'http://example.com/login'
    mail(to: @user.email, subject: "New project created #{@project.title}")
  end

end
