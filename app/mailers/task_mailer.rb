class TaskMailer < ApplicationMailer

  def task_status_updated(task_title, user_id, updated_by_id)
    @user = User.find_by(id: user_id)
    if user_id != updated_by_id
      @user_updated_by = User.find_by(id: updated_by_id)
      mail(to: @user_updated_by.email,
           subject: "Task #{task_title} updated by #{@user_updated_by.email}")
    end
    mail(to: @user.email, subject: "Task #{task_title} updated")
  end
end
