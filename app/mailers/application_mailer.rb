class ApplicationMailer < ActionMailer::Base
  default from: 'test.deveducation@example.com'
  layout 'mailer'
  def new_registration(user)
    @user = user
    mail(to: user.email, subject: "New User Signup: #{@user.email}")
  end
end
