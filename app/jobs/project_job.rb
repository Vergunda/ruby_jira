class ProjectJob < ApplicationJob
  queue_as :default

  def perform(user, project)
    puts user
    puts project
    ProjectMailer.new_project_created(user, project).deliver
    puts 'Mail_sent'
  end
end
