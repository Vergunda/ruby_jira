class SendUpdateTaskEmailJob < ApplicationJob
  queue_as :default

  def perform(task_title, user_id, updated_by_id)
    sleep 30
    TaskMailer.task_status_updated(task_title, user_id, updated_by_id).deliver
    puts 'Mail_sent'
  end
end
