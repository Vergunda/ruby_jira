class TestJob < ApplicationJob
  queue_as :low_priority

  def perform(*args)
    sleep 30
    puts 'test job done'
  end
end
